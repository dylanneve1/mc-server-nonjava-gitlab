#include "ShaderConstants.fxh"

#include "function/math.hlsl"
#include "function/grain.hlsl"

struct PS_Input
{
    float4 position : SV_Position;
    float4 color : COLOR;
    float2 skyPosition : SPOS;
};

struct PS_Output
{
    float4 color : SV_Target;
};

ROOT_SIGNATURE
void main(in PS_Input PSInput, out PS_Output PSOutput)
{
    float4 clr = PSInput.color;
    float3 cloud = float3(1.0, 1.0, 1.0);
    //clr.rgb = lerp(clr.rgb, cloud, grain(PSInput.skyPosition, TOTAL_REAL_WORLD_TIME));
    PSOutput.color = clr;
}