float3 lightSource(float3 C, float I) {
	return C * min(max(dot(I - 0.2, 0.5), 0.1), max(dot(I, 0.6), 0.0) * 0.4);
}

float3 recolor(float3 R, float M) {
	float3 newR = R;
	newR.y *= ((2.0 + M) / 4.0);
	newR.z *= max(M - 0.7, 0.0);
	
	return newR;
}