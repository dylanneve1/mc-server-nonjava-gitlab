float bpfractP3(float3 F) {
	return frac(F.x * F.y * F.z);
}

float linearize3x1(float3 l) {
    return l.x * l.y * l.z;
}

float3 polarize3(float3 P3) {
    float newP3 = (P3.r + P3.g + P3.b) / 3.0;
    return float3(newP3, newP3, newP3);
}

float unipolar(float U) {
    return 0.5 * U + 0.5;
}

float3 unipolar3(float3 U3) {
    return float3(0.5, 0.5, 0.5) * U3 + float3(0.5, 0.5, 0.5);
}

float bipolar(float B) {
    return 2.0 * B - 1.0;
}

float3 smootherstep(float3 N) {
	float3 smthrStep = N * N * N * (N * (N * float3(6.0,6.0,6.0) - float3(15.0,15.0,15.0)) + float3(10.0,10.0,10.0));
	smthrStep = clamp(smthrStep, 0.0, 1.0);

	return smthrStep;
}

float ranging(float oldValue, float oldMax, float oldMin, float newMax, float newMin) {
	float oldRange = oldMax - oldMin;
	float newRange = newMax - newMin;
	float newValue = oldValue;

	if (oldRange == 0.0) {
		newValue = newMin;
	} else {
		newValue = (((oldValue - oldMin) * newRange) / oldRange) + newMin;
	}

	return newValue;
}