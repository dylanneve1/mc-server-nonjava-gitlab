//Playing Grains
//Officially called the "Genghar's Cuatom Pseudo Random Noise".
//Yes, CUATOM. Called so from my misspelled "custom". Sounds deliberate eh?

//Typical Perlin noise
float Noise(float2 C) {
    return frac(sin(dot(C, float2(12.9898, 78.233))) * 43758.5453123);
}
/*
float Grain(float samp, float oct, float rep, float mov) {
    float sampleRepeat = mov + (samp * rep);
	float sampleOctave = mov + (samp * oct);

	float newFract = 2.0 * (sampleRepeat - round(sampleRepeat));
	float renewFract = newFract * 2.0 * (sampleOctave - round(sampleOctave));
	float reSync = sin(samp) * sin(sampleOctave);

	return (renewFract * reSync);
}

float oldGrain(float2 I, float M) {
    float GB = frac(sin(dot(I, float2(2.1, 1.3))) * 42610.429);
    float GL = frac(cos(I.x * I.y));
    float granular = 0.002 * dot(I, float2(1.0,1.0));

    float R = sin(M);
    R += (GB * GL) + (GB - GL);
    R -= GB * granular;

    return granular * R * R * (2.0 * R);
}
*/
float grain(float2 I, float M) {
	float GB = frac(sin(dot(I, float2(12.1, 71.3))) * 42610.429);//normal Perlin Noise.
	float granular = 1.0 * dot(I, float2(sin(M * 1.4), sin(M * -4.3)));

	float R = 0.5*sin(M)-0.5;
	R += GB + GB * granular;

	return clamp(R, 0.0, 1.0);
}

float sineWave(float dimension, float amplitude, float frequency, float phase) {
	return amplitude * sin(frequency * (dimension + phase));
}