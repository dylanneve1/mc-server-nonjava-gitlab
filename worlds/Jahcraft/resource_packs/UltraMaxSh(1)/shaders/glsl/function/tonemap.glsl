vec3 GMap(vec3 CI) {
	/*First iteration, original.
	vec3 CF = vec3(0.3, 0.2, 0.0);
	float colorP = max(dot(CF, CI), 0.5);
	float colorS = pow(dot(CF, 1.0), 1.0);
	CF = CF * vec3(colorP);
	CF = CF * vec3(colorS);
	CF = CI * CF;

	CI = CI * max(CF, CI);
	CI *= 2.0;*/

	return CI;
}

vec3 GBias(vec3 x) {
	//Second iteration, replace the first one for unsupported device.
	float A = 0.6;
	float B = 0.4;
	float C = 0.8;
	float D = 0.65;
	float E = 0.9;
	float F = 1.2;

	vec3 film = vec3(0.4, 0.3, 0.0);
	vec3 colorize = vec3(0.4, 0.3, 0.0);

	float newFilm = dot(film, film * x);
	vec3 newColor = colorize * newFilm;

	x = x * (x + newColor);
	x = clamp(x, 0.0, 1.0);
	x *= 3.0;

	vec3 GengharFilmic = -((x * ((A + B) / (x + C)) + D) / (x * (A / B) + (A * E))) + F;

	return GengharFilmic;
}

vec3 recurve(vec3 x) {
	/*Third iteration, replace second one for better brightness control.
	vec3 ReCurve = log((((x * abs(x + 0.16) / (pow(2.0, x) / 4.8)) * ((x * pow(2.0, 1.0)) / (pow(4.0, x)))) / pow(0.16, x)) + 1.0);
	*/

	return x;
}

vec3 smootherstep(vec3 N) {
	//Forth iteration, SmootherStep, smoother than SmoothStep.
	vec3 SmootherStep = N * N * N * (N * (N * vec3(6.0) - vec3(15.0)) + vec3(10.0));
	SmootherStep = clamp(SmootherStep, 0.0, 1.0);

	return SmootherStep;
}