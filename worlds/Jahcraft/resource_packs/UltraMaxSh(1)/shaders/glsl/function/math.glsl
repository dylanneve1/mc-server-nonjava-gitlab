float newFract(float I) {
	return 2.0 * (I - round(I));
}

float newFract(vec2 I) {
	float newI = dot(I.x, I.y);
	return 2.0 * (newI - round(newI));
}

float linearize2x1(vec2 x2) {
	float L1 = (x2.x * x2.y);
	return L1;
}

float linearize3x1(vec3 x3) {
	float L1 = (x3.r * x3.g * x3.b);
	return L1;
}

vec3 linearize3(vec3 x) {
	float L = (x.r * x.g * x.b);
	return vec3(L);
}

vec3 polarize3(vec3 P) {
	P /= 3.0;
	return vec3(P.r + P.g + P.b);
}

float unipolar(float U) {
	return 0.5 * U + 0.5;
}

vec3 unipolar3(vec3 U3) {
	return vec3(0.5) * U3 + vec3(0.5);
}

float bipolar(float B) {
	return 2.0 * B - 1.0;
}

float range(float oldValue, float oldMax, float oldMin, float newMax, float newMin) {
	float oldRange = oldMax - oldMin;
	float newRange = newMax - newMin;
	float newValue;

	if (oldRange == 0.0) {
		newValue = newMin;
	} else {
		newValue = (((oldValue - oldMin) * newRange) / oldRange) + newMin;
	}

	return newValue;
}
/*
vec3 range3(vec3 oldValue, vec3 oldMax, vec3 oldMin, vec3 newMax, vec3 newMin) {
	vec3 oldRange = oldMax - oldMin;
	vec3 newRange = newMax - newMin;
	vec3 newValue;

	if (oldRange.x == 0.0 || oldRange.y == 0.0 || oldRange.z == 0.0) {
		newValue = newMin;
	} else {
		newValue = (((oldValue - oldMin) * newRange) / oldRange) + newMin;
	}

	return newValue;
}/**/

float sineWave(float dimension, float amplitude, float frequency, float phase) {
	return amplitude * sin(frequency * (dimension + phase));
}