//Playing Grains
//Officially called the "Genghar's Cuatom Pseudo Random Noise".
//Yes, CUATOM. Called so from my misspelled "custom". Sounds deliberate eh?

float Grain(float sampler, float octave, float repeat, float movement) {
	float sampleRepeat = movement + (sampler * repeat);
	float sampleOctave = movement + (sampler * octave);

	float newFract = 2.0 * (sampleRepeat - round(sampleRepeat));
	float renewFract = newFract * 2.0 * (sampleOctave - round(sampleOctave));
	float reSync = sin(sampler) * sin(sampleOctave);

	return (renewFract * reSync);
}

float Grain2(vec2 sampler, float octave, float repeat, float movement) {
	float sampleRepeat = movement;

	return sampleRepeat;
}

float oldGrain(vec2 I, float M) {
	float GB = fract(sin(dot(I, vec2(2.1, 1.3))) * 42610.429);
	float GL = fract(cos(I.x * I.y));
	float granular = 0.002 * dot(I, vec2(1.0));

	float R = sin(M);
	R += (GB * GL) + (GB - GL);
	R -= GB * granular;

	return granular * R * R * (2.0 * R);
}